package clastarisator

import "math"

// DistanceFunc return distance between two points
type Distancer interface {
	Distance(point1, point2 []float64) float64
	MiddlePoint(points ...[]float64) []float64
}

type DecartDistancer struct{}

func (d DecartDistancer) Distance(point1, point2 []float64) float64 {
	var squareOfDistance float64
	for i := 0; i < len(point1); i++ {
		squareOfDistance += math.Pow(point1[i]-point2[i], 2)
	}
	return math.Sqrt(squareOfDistance)
}

func (d DecartDistancer) MiddlePoint(points ...[]float64) []float64 {
	if len(points) == 0 {
		return nil
	}
	result := make([]float64, len(points[0]))
	for _, point := range points {
		for pointIndex := range point {
			result[pointIndex] += point[pointIndex] / float64(len(points))
		}
	}

	return result
}
