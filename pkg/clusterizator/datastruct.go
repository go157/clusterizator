package clastarisator

import "context"

// Clusterizator give clasterisation of points
type Clusterizator interface {
	Solve(
		ctx context.Context,
		points []*Point,
		settings CentroidClusterizatorSettings,
	) ([][]*Point, error)
}

// Point in space
type Point struct {
	Coords []float64
}
