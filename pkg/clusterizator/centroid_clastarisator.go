package clastarisator

import (
	"context"
)

type centroidClusterizator struct {
	distancer Distancer
}

type clusterizationVariant struct {
	variant          map[*Point]int
	variantByCluster [][]*Point
	centroids        []centroid
	silhouette       float64
	mostDistantPoint *Point
}

type centroid struct {
	coords []float64
}

type CentroidClusterizatorSettings struct {
	MaxNumberOfClusters int
}

func NewCentroidClusterizator(
	distancer Distancer,
) Clusterizator {
	return &centroidClusterizator{
		distancer: distancer,
	}
}

func (c *centroidClusterizator) Solve(
	ctx context.Context,
	points []*Point,
	settings CentroidClusterizatorSettings,
) ([][]*Point, error) {
	maxNumberOfClusters := settings.MaxNumberOfClusters
	if maxNumberOfClusters == 0 {
		maxNumberOfClusters = len(points) / 3
	}

	variants, err := c.findVariants(
		ctx,
		points,
		settings,
		maxNumberOfClusters,
	)
	if err != nil {
		return nil, err
	}

	lastVariant := variants[len(variants)-1]

	return lastVariant.variantByCluster, nil
}

func (c *centroidClusterizator) findVariants(
	ctx context.Context,
	points []*Point,
	settings CentroidClusterizatorSettings,
	maxNumberOfClusters int,
) ([]clusterizationVariant, error) {
	result := []clusterizationVariant{}

	for i := 0; i < maxNumberOfClusters; i++ {
		currentVariant := clusterizationVariant{}

		// define clusters
		if i == 0 {
			currentVariant.centroids = []centroid{
				{
					coords: points[0].Coords,
				},
			}
		} else {
			prevCentroids := result[len(result)-1].centroids
			currentVariant.centroids = make([]centroid, len(prevCentroids)+1)
			copy(currentVariant.centroids, prevCentroids)
			currentVariant.centroids[len(prevCentroids)] = centroid{
				coords: result[len(result)-1].mostDistantPoint.Coords,
			}
		}

		// rebalance
		hasChanges := true
		currentVariant.variant = make(map[*Point]int, len(points))
		for hasChanges {
			select {
			case <-ctx.Done():
				if len(result) == 0 {
					return nil, ctx.Err()
				} else {
					return result, nil
				}
			default:
				// set points to clusters
				newVariant := make(map[*Point]int, len(points))
				var maxDistance float64
				for _, point := range points {
					var currentCentroid int
					var minDistanceForPoint float64
					for centroidID, centroid := range currentVariant.centroids {
						clusterDistanceForPoint := c.distancer.Distance(point.Coords, centroid.coords)
						if centroidID == 0 || clusterDistanceForPoint < minDistanceForPoint {
							currentCentroid = centroidID
							minDistanceForPoint = clusterDistanceForPoint
						}
					}
					newVariant[point] = currentCentroid
					if minDistanceForPoint > maxDistance {
						currentVariant.mostDistantPoint = point
					}
				}

				hasChanges = false
				pointsOfCentroid := make(map[int][]*Point, len(currentVariant.centroids))
				for _, point := range points {
					if !hasChanges && newVariant[point] != currentVariant.variant[point] {
						hasChanges = true
					}

					pointsOfCentroid[newVariant[point]] = append(pointsOfCentroid[newVariant[point]], point)
				}
				currentVariant.variant = newVariant

				// move centroids
				for centroidID := range currentVariant.centroids {
					centroidPoints := pointsOfCentroid[centroidID]
					centroidPointsCoords := make([][]float64, len(centroidPoints))
					for i, point := range centroidPoints {
						centroidPointsCoords[i] = point.Coords
					}
					currentVariant.centroids[centroidID].coords = c.distancer.MiddlePoint(centroidPointsCoords...)
				}
			}
		}

		variantByCluster := make([][]*Point, len(currentVariant.centroids))
		for point, clusterID := range currentVariant.variant {
			variantByCluster[clusterID] = append(variantByCluster[clusterID], point)
		}
		currentVariant.variantByCluster = variantByCluster

		currentVariant.silhouette = c.calcSilhouette(currentVariant)
		if i > 0 && currentVariant.silhouette < result[i-1].silhouette {
			break
		}

		result = append(result, currentVariant)
	}

	return result, nil
}

func (c *centroidClusterizator) calcSilhouette(currentVariant clusterizationVariant) float64 {
	var result float64

	if len(currentVariant.centroids) == 1 {
		return 0
	}

	for centroidID := range currentVariant.centroids {
		for pointID, point := range currentVariant.variantByCluster[centroidID] {
			var closestCentroidDistance float64
			var averageInnerDistance float64

			pointsInCluster := len(currentVariant.variantByCluster[centroidID])

			if pointsInCluster <= 1 {
				break
			}

			for possibleClosestCentroidID, possibleCentroid := range currentVariant.centroids {
				if possibleClosestCentroidID == centroidID {
					continue
				}

				possibleDistance := c.distancer.Distance(point.Coords, possibleCentroid.coords)

				if closestCentroidDistance == 0 || possibleDistance < closestCentroidDistance {
					closestCentroidDistance = possibleDistance
				}
			}

			for sameClusterPointID, sameClusterPoint := range currentVariant.variantByCluster[centroidID] {
				if sameClusterPointID == pointID {
					continue
				}

				averageInnerDistance += c.distancer.Distance(sameClusterPoint.Coords, point.Coords) / float64(pointsInCluster-1)
			}

			result += (closestCentroidDistance - averageInnerDistance) / closestCentroidDistance
		}
	}

	result /= float64(len(currentVariant.centroids) - 1)
	return result
}
