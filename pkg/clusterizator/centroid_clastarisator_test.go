package clastarisator

import (
	"context"
	"sort"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_centroidClusterizator_Solve(t *testing.T) {
	t.Parallel()

	type fields struct {
		points    []*Point
		distancer Distancer
		settings  CentroidClusterizatorSettings
	}

	type testCase struct {
		name    string
		fields  fields
		want    [][]*Point
		wantErr bool
	}

	tests := []testCase{
		func() testCase {
			points := []*Point{
				{
					Coords: []float64{10, 6},
				},
				{
					Coords: []float64{12, 5},
				},
				{
					Coords: []float64{9, 9},
				},
				{
					Coords: []float64{45, 43},
				},
				{
					Coords: []float64{50, 47},
				},
				{
					Coords: []float64{50, 43},
				},
				{
					Coords: []float64{45, 47},
				},
			}

			return testCase{
				name: "2 groups",
				fields: fields{
					points:    points,
					distancer: DecartDistancer{},
					settings:  CentroidClusterizatorSettings{},
				},
				want: [][]*Point{
					{points[0], points[1], points[2]},
					{points[3], points[4], points[5], points[6]},
				},
			}
		}(),
		func() testCase {
			points := []*Point{
				{
					Coords: []float64{10, 6},
				},
				{
					Coords: []float64{12, 5},
				},
				{
					Coords: []float64{9, 9},
				},
				{
					Coords: []float64{45, 43},
				},
				{
					Coords: []float64{50, 47},
				},
				{
					Coords: []float64{50, 43},
				},
				{
					Coords: []float64{45, 47},
				},
				{
					Coords: []float64{48, 49},
				},
				{
					Coords: []float64{49, 48},
				},
				{
					Coords: []float64{7, 6},
				},
				{
					Coords: []float64{8, 9},
				},
				{
					Coords: []float64{9, 8},
				},
			}

			return testCase{
				name: "big set of 2 groups",
				fields: fields{
					points:    points,
					distancer: DecartDistancer{},
					settings:  CentroidClusterizatorSettings{},
				},
				want: [][]*Point{
					{points[3], points[4], points[5], points[6], points[7], points[8]},
					{points[0], points[1], points[2], points[9], points[10], points[11]},
				},
			}
		}(),
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			solver := NewCentroidClusterizator(
				tt.fields.distancer,
			)
			result, err := solver.Solve(
				context.Background(),
				tt.fields.points,
				tt.fields.settings,
			)
			sortClusters(result)
			sortClusters(tt.want)
			require.Equal(t, tt.wantErr, err != nil)
			require.Equal(t, tt.want, result)
		})
	}
}

func sortClusters(points [][]*Point) {
	for i := range points {
		sort.SliceStable(points[i], func(a, b int) bool {
			for coordIndex := 0; coordIndex < len(points[i][a].Coords); coordIndex++ {
				if points[i][a].Coords[coordIndex] == points[i][b].Coords[coordIndex] {
					continue
				}

				return points[i][a].Coords[coordIndex] < points[i][b].Coords[coordIndex]
			}

			return false
		})
	}
}
